#### Prueba 1 - Diagrama de Red

El diagrama de red se encuentra en la carpeta Diagrama-red en https://gitlab.com/alexis-petrovich/craftech
Se demuestran 2 vpcs + un entorno on-premise. La vpc1 contiene la app en Amazon Elastic Container Service, distribuida con una replica en cada AZ. La app se consume a traves de un ALB que tiene por delante el servicio de AWS Shield como proteccion ante ataques externos.
La vpc2 contiene instancias EC2 donde podrian estar las bases de datos no expuestas. Un Transit Gatway hace la conexion via attachment entre ambas VPC y tambien via VPN hacia un datacenter on-premise para consumir de servicios alli.

#### Prueba 2 - Despliegue de una aplicación Django y React.js

Ubicacion - https://gitlab.com/alexis-petrovich/craftech

-El dockerfile del backend se encuentra en app-backend/dockerfile

-El dockerfile del backend se encuentra en app-frontend/dockerfile

-El archivo gitlab_ci.yml en la raiz del proyecto buildea/dockeriza ambas imagenes, pudiendo setearse en automatico o manual el pipeline

-Las imagenes son alojadas en registry.gitlab.com/alexis-petrovich/craftech:frontend-craftech y registry.gitlab.com/alexis-petrovich/craftech:backend-craftech

-En la carpeta /compose se encuentra el docker-compose.yml que levanta los servicios de base, el front y el backend

-Se aloja docker-compose.yml en https://gitlab.com/alexis-petrovich/compose para evitar necesidad de clonar codigo fuente completo segun la prueba necesaria

**Despliegue local** - 

1. git clone https://gitlab.com/alexis-petrovich/craftech.git 
2. cd craftech-master/compose/
3. docker-compose up --detach .


Despligue en AWS - 

_-se baso en EC2 ami ubuntu-focal-20.04-amd64-server_

1. sudo snap install docker (instalar docker y docker-compose)
2. sudo docker clone https://gitlab.com/alexis-petrovich/compose (obtener docker-compose.yml)
3. cd /compose/
4. sudo docker-compose up -d

#### Prueba 3 - CI/CD

Ubicacion https://gitlab.com/alexis-petrovich/customnginx

-El index.html es contenido en la carpeta /html

-El dockerfile copia los contenidos de nuestro custom index a la ubicacion default del index.htm de nginx

-El archivo gitlab-ci.yml buildea la imagen al realizar cambios al index.html y la aloja en registry.gitlab.com/alexis-petrovich/customnginx:customnginx




